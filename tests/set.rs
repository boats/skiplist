#[macro_use] extern crate proptest;
extern crate rayon;
extern crate skiplist;

mod single_threaded {

    use proptest::prelude::*;
    use skiplist::GrowSet;

    proptest! {
        #[test]
        fn insert_values(ref test_set in prop::collection::hash_set(".*", 0..10)) {
            // test_set: std::collections::HashSet<&String>
            // test_set.len() ∈ [0,9]

            let grow_set: GrowSet<String> = GrowSet::new();
            for v in test_set {
                grow_set.insert(v.clone());
            }
            for v in test_set {
                prop_assert!(grow_set.contains(v));
            }
            prop_assert_eq!(test_set.len(), grow_set.len());
        }
    }

}

mod multi_threaded {

    use proptest::prelude::*;
    use rayon::prelude::*;
    use skiplist::GrowSet;

    proptest! {
        #[test]
        fn insert_values(ref test_set in prop::collection::hash_set(".*", 0..4)) {
            // test_set: std::collections::HashSet<&String>
            // test_set.len() ∈ [0,3]

            // Never seems to halt. I saw that the multi_threaded
            // tests in src/skiplist/tests.rs wrap the GrowSet in
            // Arc. I didn't do it here, because the compiler isn't
            // complaining. If it's an error to reference the grow_set
            // from within a rayon closure, then this code should not
            // compile. Maybe this could be achieved by not allowing
            // GrowSet to be Sync?
            //
            // This test DOES halt and pass when test_set.len() ∈ [0,2].
            // I think that's just because rayon never dispatches work
            // to any other thread in that case.

            let grow_set: GrowSet<String> = GrowSet::new();

            test_set.par_iter().for_each(|v| { grow_set.insert(v.clone()); });
            for v in test_set {
                prop_assert!(grow_set.contains(v));
            }
            prop_assert_eq!(test_set.len(), grow_set.len());
        }
    }

}
