use std::borrow::Borrow;
use std::iter::FromIterator;

use skiplist::{SkipList, Iter as InnerIter, IntoIter as InnerIntoIter};

pub struct GrowSet<T> {
    skiplist: SkipList<T>,
}

impl<T> GrowSet<T> {
    pub fn new() -> GrowSet<T> {
        GrowSet { skiplist: SkipList::new() }
    }

    pub fn len(&self) -> usize {
        self.skiplist.len()
    }

    pub fn is_empty(&self) -> bool {
        self.skiplist.is_empty()
    }

    pub fn iter(&self) -> Iter<T> {
        self.into_iter()
    }
}

impl<T: Ord> GrowSet<T> {
    pub fn insert(&self, elem: T) -> Option<T> {
        self.skiplist.insert(elem, <T as Ord>::cmp)
    }
    pub fn contains<Q>(&self, key: &Q) -> bool where
        Q: ?Sized + Ord + Eq,
        T: Borrow<Q>,
    {
        self.skiplist.find(|elem| key.cmp(elem.borrow())).is_some()
    }

    pub fn get<Q>(&self, key: &Q) -> Option<&T> where
        Q: ?Sized + Ord + Eq,
        T: Borrow<Q>,
    {
        self.skiplist.find(|elem| key.cmp(elem.borrow()))
    }
}

pub struct Iter<'a, T: 'a> {
    inner: InnerIter<'a, T>,
}

pub struct IntoIter<T> {
    inner: InnerIntoIter<T>,
}

impl<T> IntoIterator for GrowSet<T> {
    type Item = T;
    type IntoIter = IntoIter<T>;

    fn into_iter(self) -> IntoIter<T> {
        IntoIter {
            inner: self.skiplist.into_iter(),
        }
    }
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

impl<'a, T> IntoIterator for &'a GrowSet<T> {
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Iter<'a, T> {
        Iter {
            inner: self.skiplist.iter(),
        }
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

impl<T: Ord> FromIterator<T> for GrowSet<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> GrowSet<T> {
        let set = GrowSet::new();
        for elem in iter {
            set.insert(elem);
        }
        set
    }
}

impl<T: Ord> Extend<T> for GrowSet<T> {
    fn extend<I: IntoIterator<Item = T>>(&mut self, iter: I) {
        for elem in iter {
            self.insert(elem);
        }
    }
}

#[cfg(feature = "rayon")]
mod rayon {
    use rayon::iter::{FromParallelIterator, IntoParallelIterator, ParallelIterator, ParallelExtend};

    use super::GrowSet;

    impl<T: Ord + Send + Sync> FromParallelIterator<T> for GrowSet<T> {
        fn from_par_iter<I: IntoParallelIterator<Item = T>>(par_iter: I) -> GrowSet<T> {
            let set = GrowSet::new();
            par_iter.into_par_iter().for_each(|elem| {
                set.insert(elem);
            });
            set
        }
    }

    impl<T: Ord + Send + Sync> ParallelExtend<T> for GrowSet<T> {
        fn par_extend<I: IntoParallelIterator<Item = T>>(&mut self, par_iter: I) {
            par_iter.into_par_iter().for_each(|elem| {
                self.insert(elem);
            });
        }
    }
}
