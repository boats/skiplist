mod iter;
mod node;
mod ptr;
#[cfg(test)] mod tests;

use std::cmp::{self, Ordering};
use std::mem;
use std::ptr::read as ptr_read;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::*;

pub use self::iter::{Iter, IntoIter};

use self::node::Node;
use self::ptr::Ptr;

const MAX_HEIGHT: usize = 31;

fn random_height() -> usize {
    const MASK: u32 = 1 << MAX_HEIGHT;
    1 + (::rand::random::<u32>() | MASK).trailing_zeros() as usize
}

#[derive(Debug)]
pub struct SkipList<T> {
    lanes: [Ptr<T>; MAX_HEIGHT],
    height: AtomicUsize,
}

impl<T> SkipList<T> {
    pub fn new() -> SkipList<T> {
        unsafe { mem::zeroed() }
    }

    pub fn len(&self) -> usize {
        let mut ctr = 0;
        let mut ptr = unsafe { self.lanes.get_unchecked(0) };
        while let Some(node) = ptr.read() {
            ctr += 1;
            ptr = unsafe { node.lanes().get_unchecked(0) };
        }
        ctr
    }

    pub fn is_empty(&self) -> bool {
        self.height.load(Relaxed) == 0
    }

    pub fn find<F>(&self, cmp: F) -> Option<&T>
        where F: Fn(&T) -> Ordering
    {
        let height = self.height.load(Relaxed);
        let mut lanes: &[Ptr<T>] = unsafe { &self.lanes.get_unchecked(..height) };

        for height in (0..height).rev() {
            while let Some(node) = unsafe { lanes.get_unchecked(height).read() } {
                let elem = node.elem();
                match cmp(elem) {
                    Ordering::Less      => break,
                    Ordering::Equal     => return Some(elem),
                    Ordering::Greater   => lanes = node.lanes(),
                }
            }
        }
        None
    }

    pub fn insert<F>(&self, elem: T, cmp: F) -> Option<T>
        where F: Fn(&T, &T) -> Ordering
    {
        let node_height = random_height();

        let mut preds: [&Ptr<T>; MAX_HEIGHT] = unsafe { mem::uninitialized() };
        let mut succs: [Option<&Node<T>>; MAX_HEIGHT] = unsafe { mem::uninitialized() };
        let mut min_height = 0;
        let mut ptr: Ptr<T> = unsafe { mem::zeroed() };

        'locking: loop {
            let current_height = self.height.load(Acquire);
            let max_height = cmp::max(node_height, current_height);
            let mut lanes: &[Ptr<T>] = unsafe { &self.lanes.get_unchecked(..max_height) };

            // Search
            for height in (min_height..max_height).rev() {
                unsafe {
                    loop {
                        let pred: &Ptr<T> = lanes.get_unchecked(height);
                        if let Some(node) = pred.read() {
                            match cmp(&elem, node.elem()) {
                                Ordering::Equal     => return Some(elem),
                                Ordering::Greater   => lanes = node.lanes(),
                                Ordering::Less      => {
                                    *preds.get_unchecked_mut(height) = pred;
                                    *succs.get_unchecked_mut(height) = Some(node);
                                    break
                                }
                            }
                        } else {
                            *preds.get_unchecked_mut(height) = pred;
                            *succs.get_unchecked_mut(height) = None;
                            break
                        }
                    }
                }
            }

            // Insert
            if ptr.addr() == 0 {
                ptr = Node::new(unsafe { ptr_read(&elem) }, node_height);
            }
            unsafe {
                let node: &Node<T> = ptr.read_unchecked();
                let iter = preds.iter().zip(&succs).enumerate().take(node_height).skip(min_height);

                for (n, (&pred, &succ)) in iter {
                    let node_addr = addr_of(node);
                    let succ_addr = succ.map_or(0, addr_of);

                    if !pred.compare_store(succ_addr, node_addr) {
                        continue 'locking;
                    }

                    if succ_addr != 0 {
                        node.lanes().get_unchecked(n).store(succ_addr);
                    }

                    min_height = n;
                }
            }

            if max_height > current_height {
                self.height.store(max_height, Release);
            }

            mem::forget(ptr);
            mem::forget(elem);
            return None;
        }
    }

    pub fn iter<'a>(&'a self) -> Iter<'a, T> {
        self.into_iter()
    }
}

impl<T> Drop for SkipList<T> {
    fn drop(&mut self) {
        unsafe {
            drop(ptr_read(self.lanes.get_unchecked(0)));
            mem::forget(mem::replace(&mut self.lanes, mem::zeroed()));
        }
    }
}

fn addr_of<T: ?Sized>(r: &T) -> usize {
    r as *const T as *const () as usize
}
