use std::marker::PhantomData;
use std::mem;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::*;
use std::usize;

use skiplist::Node;

#[derive(Debug)]
pub(crate) struct Ptr<T> {
    addr: AtomicUsize,
    _marker: PhantomData<T>,
}

impl<T> Ptr<T> {
    pub(crate) fn new(node: *mut Node<T>) -> Ptr<T> {
        Ptr {
            addr: AtomicUsize::new(node as usize),
            _marker: PhantomData,
        }
    }

    pub(crate) fn read(&self) -> Option<&Node<T>> {
        unsafe { mem::transmute(self.addr()) } 
    }

    pub(crate) unsafe fn read_unchecked(&self) -> &Node<T> {
        mem::transmute(self.addr())
    }

    pub(crate) fn addr(&self) -> usize {
        self.addr.load(Acquire)
    }

    pub(crate) fn compare_store(&self, old: usize, new: usize) -> bool {
        self.addr.compare_and_swap(old, new, Release) == old
    }

    pub(crate) fn store(&self, new: usize) {
        self.addr.store(new, Release);
    }
}

impl<T> Drop for Ptr<T> {
    fn drop(&mut self) {
        if let Some(node) = self.read() {
            unsafe {
                node.drop_next();
                node.dealloc();
            }
        }
    }
}
