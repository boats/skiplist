use std::borrow::Borrow;
use std::iter::FromIterator;

use skiplist::{SkipList, Iter as InnerIter, IntoIter as InnerIntoIter};

pub struct GrowMap<K, V> {
    skiplist: SkipList<(K, V)>,
}

impl<K, V> GrowMap<K, V> {
    pub fn new() -> GrowMap<K, V> {
        GrowMap { skiplist: SkipList::new() }
    }
    pub fn len(&self) -> usize {
        self.skiplist.len()
    }

    pub fn is_empty(&self) -> bool {
        self.skiplist.is_empty()
    }

    pub fn iter(&self) -> Iter<K, V> {
        self.into_iter()
    }
}

impl<K: Ord, V> GrowMap<K, V> {
    pub fn insert(&self, key: K, value: V) -> Option<(K, V)> {
        self.skiplist.insert((key, value), |lhs, rhs| lhs.0.cmp(&rhs.0))
    }

    pub fn contains<Q>(&self, key: &Q) -> bool where
        Q: ?Sized + Ord,
        K: Borrow<Q>,
    {
        self.skiplist.find(|elem| key.cmp(&elem.0.borrow())).is_some()
    }

    pub fn get<Q>(&self, key: &Q) -> Option<&V> where
        Q: ?Sized + Ord,
        K: Borrow<Q>,
    {
        self.skiplist.find(|elem| key.cmp(&elem.0.borrow())).map(|&(_, ref v)| v)
    }
}

pub struct Iter<'a, K: 'a, V: 'a> {
    inner: InnerIter<'a, (K, V)>,
}

pub struct IntoIter<K, V> {
    inner: InnerIntoIter<(K, V)>,
}

impl<K, V> IntoIterator for GrowMap<K, V> {
    type Item = (K, V);
    type IntoIter = IntoIter<K, V>;

    fn into_iter(self) -> IntoIter<K, V> {
        IntoIter {
            inner: self.skiplist.into_iter(),
        }
    }
}

impl<K, V> Iterator for IntoIter<K, V> {
    type Item = (K, V);

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

impl<'a, K, V> IntoIterator for &'a GrowMap<K, V> {
    type Item = (&'a K, &'a V);
    type IntoIter = Iter<'a, K, V>;

    fn into_iter(self) -> Iter<'a, K, V> {
        Iter {
            inner: self.skiplist.iter(),
        }
    }
}

impl<'a, K, V> Iterator for Iter<'a, K, V> {
    type Item = (&'a K, &'a V);

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|&(ref k, ref v)| (k, v))
    }
}

impl<K: Ord, V> FromIterator<(K, V)> for GrowMap<K, V> {
    fn from_iter<I: IntoIterator<Item = (K, V)>>(iter: I) -> GrowMap<K, V> {
        let map = GrowMap::new();
        for (key, value) in iter {
            map.insert(key, value);
        }
        map
    }
}

impl<K: Ord, V> Extend<(K, V)> for GrowMap<K, V> {
    fn extend<I: IntoIterator<Item = (K, V)>>(&mut self, iter: I) {
        for (key, value) in iter {
            self.insert(key, value);
        }
    }
}

#[cfg(feature = "rayon")]
mod rayon {
    use rayon::iter::{FromParallelIterator, IntoParallelIterator, ParallelIterator, ParallelExtend};

    use super::GrowMap;

    impl<K: Ord + Send + Sync, V: Send + Sync> FromParallelIterator<(K, V)> for GrowMap<K, V> {
        fn from_par_iter<I: IntoParallelIterator<Item = (K, V)>>(par_iter: I) -> GrowMap<K, V> {
            let map = GrowMap::new();
            par_iter.into_par_iter().for_each(|(key, value)| {
                map.insert(key, value);
            });
            map
        }
    }

    impl<K: Ord + Send + Sync, V: Send + Sync> ParallelExtend<(K, V)> for GrowMap<K, V> {
        fn par_extend<I: IntoParallelIterator<Item = (K, V)>>(&mut self, par_iter: I) {
            par_iter.into_par_iter().for_each(|(key, value)| {
                self.insert(key, value);
            });
        }
    }
}
